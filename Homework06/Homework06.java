import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

/* Задача 1. Реализовать функцию, принимающую на вход массив и целое число. Данная функция должна вернуть
             индекс этого числа в массиве. Если число в массиве отсутствует - вернуть -1.
   Задача 2. Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:
             было:
             34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20
             стало
             34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0 */

public class Homework06 {

    public static int getNumberIndex(int[] array, int a) {
        int numberIndex = -1;
        for (int i = 0; i < array.length; i++) {
            if (a == array[i]) { // сравнение введенного числа и элементов массива
                numberIndex = i;
                System.out.println(numberIndex);
            }
        }
        System.out.println("Найдены все индексы");
        if (numberIndex == -1) {
            System.out.println("Данного числа нет в массиве");
        }
        return numberIndex;
    }

    public static void sortArrayElements(int[] array) {
        int lastNumber;
        int newIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                lastNumber = array[i];
                array[i] = array[newIndex];
                array[newIndex] = lastNumber;
                newIndex++;
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static int[] getArrayElements(String str) {
        Scanner scanner = new Scanner(System.in);
        int flag = 0;
        int[] elements; // создаем числовой массив
        do {
            String[] line = str.split(" "); // Назначаем " " в качестве разделителя и записываем в строчный массив
            elements = new int[line.length]; // задаем длину числового массива
            for (int i = 0, lineLength = line.length; i < lineLength; i++) {
                try {
                    elements[i] = Integer.parseInt(line[i]); // Переводим подстроки в числа и записываем в числовой массив
                    flag = 1;
                } catch (NumberFormatException e) {
                    System.out.println("Вы ввели что-то кроме целого числа. Завершить процедуру? (да/нет)");
                    str = scanner.nextLine();
                    if (Objects.equals(str, "да")) {
                        flag = 1;
                        System.exit(0);
                    } else if (Objects.equals(str, "нет")) {
                        System.out.println("Введите целые числа через пробел для заполнения массива: ");
                        str = scanner.nextLine();
                        flag = 0;
                    }
                    break;
                }
            }
        } while (flag == 0);
        // System.out.println(Arrays.toString(elements)); // Вывод массива (ПРОВЕРКА)
        return elements;
    }

    public static int getNumber(String word) {
        Scanner scanner = new Scanner(System.in);
        int number = 0;
        int flag = 0;
        do {
            try {
                number = Integer.parseInt(word);
                flag = 1;
            } catch (NumberFormatException e) {
                System.out.println("Вы ввели что-то кроме целого числа. Завершить процедуру? (да/нет)");
                word = scanner.nextLine();
                if (Objects.equals(word, "да")) {
                    flag = 1;
                    System.exit(0);
                } else if (Objects.equals(word, "нет")) {
                    System.out.println("Введите целое число, индекс которого необходимо найти: ");
                    word = scanner.nextLine();
                    flag = 0;
                }
            }
        } while (flag == 0);
        // System.out.println(number); // Вывод числа (ПРОВЕРКА)
        return (number);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите 1 (решение задачи 1) или 2 (решение задачи 2): ");
        String chooseSolution = scanner.nextLine();
        if (chooseSolution.equals("1")) {
            System.out.println("Введите целые числа через пробел для заполнения массива: ");
            String finalStr = scanner.nextLine();
            int[] finalElements = getArrayElements(finalStr);
            System.out.println("Введите целое число, индекс которого необходимо найти: ");
            String finalWord = scanner.nextLine(); // Читаем с клавиатуры текст
            int finalNumber = getNumber(finalWord);
            getNumberIndex(finalElements, finalNumber);
        } else if (chooseSolution.equals("2")) {
            System.out.println("Введите целые числа через пробел для заполнения массива: ");
            String finalStr = scanner.nextLine();
            int[] finalElements = getArrayElements(finalStr);
            //System.out.println(finalElements); ПРОВЕРКА
            sortArrayElements(finalElements);
        }
    }
}
